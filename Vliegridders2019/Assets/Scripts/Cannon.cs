﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    public ParticleSystem cannonFire;
    public ParticleSystem cannonSmoke;
    public Transform cannonEnd;
    public GameObject knightPrefab;
    public static bool shooting;
    
    public int knightsShot;
    private int power;
    public int powerMax;
    public int powerMin;
    bool increasing = true;
    public int incrementer = 1;

    //[HideInInspector] public KnightManager[] knights;
    public CameraControl m_CameraControl;
    public Slider powerMeter;


    // Use this for initialization
    void Start()
    {
        powerMeter.maxValue = powerMax;

    }

    void Update()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        if (!shooting)
        {
            if (power <= 5)
                increasing = true;
            if (power >= 100)
                increasing = false;

            if (increasing)
            {
                power += incrementer;
            }
            else
            {
                power -= incrementer;
            }
            powerMeter.value = power;
        }
    }



    public void Fire()
    {
        ParticleSystem.Instantiate(cannonSmoke, cannonEnd.position, cannonEnd.rotation);
        ParticleSystem.Instantiate(cannonFire, cannonEnd.position, cannonEnd.rotation);
        shooting = true;

        Transform[] cameraTargets = new Transform[knightsShot];

        for (int i = 0; i < knightsShot; i++)
        {
            float halflist = knightsShot / 2;
            GameObject knight = (GameObject)Instantiate(
                knightPrefab,
                cannonEnd.position + new Vector3(cannonEnd.right.x - (0.5f * halflist) + (0.50f * i), 0, 0),
                cannonEnd.rotation);
            knight.GetComponent<Rigidbody>().velocity = knight.transform.forward * ((powerMeter.value / 10) * ShopManager.cannonPower);
            cameraTargets[i] = knight.transform;
            GameManager.knightsLeft--;

        }

        m_CameraControl.SetCameraTargets(cameraTargets);


        /*
        for (int i = 0; i < knights.Length; i++)
        {
            // ... create them, set their player number and references needed for control.
            knights[i].m_Instance =
                Instantiate(knightPrefab, cannonEnd.position, cannonEnd.rotation) as GameObject;
            //knights[i].GetComponent<Rigidbody>().velocity = knights[i].transform.right * 15;
            //knights[i].m_PlayerNumber = i + 1;
            //knights[i].Setup();
            */
    }
}

