﻿using UnityEngine;
using System.Collections;

public class Explosive : MonoBehaviour {


    public float radius;
    public float upwardLift;
    public float forwardPush;
    private Vector3 addedForce;
    public ParticleSystem blast;
    public ParticleSystem chunks;


    // Use this for initialization
    void Start () {
        addedForce = new Vector3(forwardPush, upwardLift);
	}

    void OnDrawGizmos()//radius van de explosie
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
    public void Explode()//blaast ridders weg en explodeert huisjes
    {
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            if (hit.gameObject.CompareTag("Settlement"))//dit blaast de huisjes op
            {
                hit.gameObject.GetComponent<Village>().Destroy();
            }

            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
            {
                if(rb.velocity.y < 0f)//als de ridder naar beneden valt, word hij eerst stopgezet voordat force word toegevoegd
                {
                    rb.velocity = new Vector3(rb.velocity.x, 0, 0);
                }
                rb.AddForce(addedForce * (1f -((Vector3.Distance(rb.position, explosionPos)) / radius)));
            }
        }
        ParticleSystem.Instantiate(blast, transform.position, Quaternion.Euler(-90,0,0));
        ParticleSystem.Instantiate(chunks, transform.position, Quaternion.Euler(-90, 0, 0));
        gameObject.SetActive(false);
    }

	// Update is called once per frame
	void Update () {
	
	}
}
