﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour
{

    public Text scoreText;
    public Text knightsText;
    public Text rocketsText;
    public Text endOfRoundText;
    private int tempScore;
    public WorldSpawner worldSpawner;
    public static int score;
    public static int knightsLeft;
    private int rocketsLeft;
    

    public GameObject rocket;
    public GameObject shopPanel;
    public GameObject endOfRoundPanel;
    public GameObject endRoundButton;
    public Cannon cannon;
    public CameraControl cameraControl;
    private Camera mainCamera;
    
    public int startRockets;//for testing purposes :V
    public int startKnights;
    private bool canNuke;
    // private float count;

    GameObject[] settlements;
    GameObject[] explosives;
    // Use this for initialization

    void Start()
    {
        canNuke = false;
        mainCamera = cameraControl.GetComponentInChildren<Camera>();
        RefillRockets();
        knightsLeft = startKnights;
        //worldBuilder.GetComponent<>();
        settlements = GameObject.FindGameObjectsWithTag("Settlement");
        explosives = GameObject.FindGameObjectsWithTag("Explosive");

        score = 0;
        endRoundButton.SetActive(false);
        shopPanel.SetActive(false);
        endOfRoundPanel.SetActive(false);
    }



    // Update is called once per frame
    public void Update()
    {
        UpdateText();

        Vector3 mouse = Input.mousePosition;
        Vector3 screenPoint = Camera.main.WorldToScreenPoint(cannon.transform.localPosition);
        Vector2 offset = new Vector2(mouse.x - screenPoint.x, mouse.y - screenPoint.y);
        float angle = Mathf.Clamp((Mathf.Atan2(offset.y, offset.x) * Mathf.Rad2Deg),-5 ,85);//cannon doesn't shoot higher than 85 degrees, or lower than -5.  0 degrees = -->

        if (!Cannon.shooting)
        {
            cannon.transform.rotation = Quaternion.Euler(0, 0, angle);
        }

        if (Input.GetMouseButtonDown(0) && !Cannon.shooting)
        {
            cannon.Fire();
            Gameplay();
        }
        else if (Input.GetMouseButtonDown(0) && canNuke && rocketsLeft > 0)
        {
            FireRocket();
        }
    }


    public void FireRocket()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        
        if (Physics.Raycast(ray, out hit, 80))
        {
            //draw invisible ray cast/vector
            //Debug.DrawLine(ray.origin, hit.point);

            //log hit area to the console
            //Debug.Log(hit.point);

            Vector3 launchPos = new Vector3(hit.point.x, hit.point.y, mainCamera.transform.position.z);
            Debug.Log("lanceerpositie = "+launchPos);
            GameObject missile = (GameObject)Instantiate(rocket, launchPos, Quaternion.Euler(90, 0, 0));
            missile.transform.parent = mainCamera.transform;
            rocketsLeft--;
        }
    }

    public void UpdateText()
    {
        scoreText.text = "Gold: " + score;
        knightsText.text = "Knights: " + knightsLeft;
        if(ShopManager.rocketCurrentUpgrade > 0)
        {
            rocketsText.text = "Missiles: " + rocketsLeft;
        }
        else
        {
            rocketsText.text = "";
        }
        
    }

    public void SetEndOfRoundText()
    {
        endOfRoundText.text = "<b>Gold earned this round:\t\t" + (score - tempScore) + 
            "\n\nDistance travelled:\t\t\t\t" + (mainCamera.transform.position.x * 10).ToString("0.0") + "m</b>";   
    }

    public void OpenShop()
    {

        endOfRoundPanel.SetActive(false);
        shopPanel.SetActive(true);
        Cannon.shooting = true;        
    }

    public void ReadyToFire()
    {
            endOfRoundPanel.SetActive(false);
            shopPanel.SetActive(false);
            canNuke = false;
            RefillRockets();
            Cannon.shooting = false;
    }

    public void EndOfRound()
    {
        canNuke = false;
        endRoundButton.SetActive(false);
        endOfRoundPanel.SetActive(true);
        SetEndOfRoundText();
        cameraControl.ResetToStartTarget();
        DestroyAllKnights();
        worldSpawner.ResetVillages();
    }

    public void Gameplay()
    {
        tempScore = score;
        endRoundButton.SetActive(true);
        canNuke = true;
    }



    public void RefillRockets()
    {
        rocketsLeft = ShopManager.rocketAmount;
    }

    public void DestroyAllKnights()
    {
        GameObject[] knights = GameObject.FindGameObjectsWithTag("Knight");
        foreach (GameObject knight in knights)
        {
            knight.gameObject.GetComponent<Knight>().Delete();
        }
    }

    public void GimmeGold()
    {
        score += 10;
    }
    /*
    public void Reset()
    {

    }
    */

}
