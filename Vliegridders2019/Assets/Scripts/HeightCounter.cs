﻿using UnityEngine;
using System.Collections;

public class HeightCounter : MonoBehaviour {

    GameObject flyingObject;
    float objectHeight;

    
    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        objectHeight = flyingObject.transform.position.y;

	    if(gameObject.GetComponent<Renderer>().enabled == true && flyingObject.GetComponent<Renderer>().isVisible)//make pointer invisible if object is on screen 
        {
            gameObject.GetComponent<Renderer>().enabled = false;
        }

        if (gameObject.GetComponent<Renderer>().enabled == false && !flyingObject.GetComponent<Renderer>().isVisible) //make pointer visible if obj is off screen
        {
            gameObject.GetComponent<Renderer>().enabled = true;
        }


    }
}
