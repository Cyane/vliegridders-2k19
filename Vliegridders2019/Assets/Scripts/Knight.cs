﻿using UnityEngine;
using System.Collections;


public class Knight : MonoBehaviour
{

    private Rigidbody rb;
    private int rnd;
    private GameObject indicator;
    public GameObject heightIndicator;
    public float screenHeight;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        indicator = (GameObject)Instantiate(heightIndicator, new Vector3(transform.position.x,screenHeight,0), Quaternion.identity);
        indicator.SetActive(false);
    }

    void FixedUpdate()
    {
        if(rb.transform.position.y > screenHeight)
        {
            indicator.gameObject.SetActive(true);
            indicator.transform.Find("TextMesh").gameObject.GetComponent<TextMesh>().text = ""+ System.Math.Round(rb.transform.position.y,1);
            indicator.transform.position = new Vector3(rb.position.x, indicator.transform.position.y, indicator.transform.position.z);

        }else if(rb.transform.position.y <= screenHeight && indicator.gameObject.activeInHierarchy)
        {
            indicator.gameObject.SetActive(false); 
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Settlement"))
        {
            Debug.Log("penetratie = " + ShopManager.penetration);
            Debug.Log("velocity = " + rb.velocity);
            Debug.Log("velocity verlies = "+(rb.velocity - (rb.velocity / other.GetComponent<Village>().Divider())));
            Debug.Log("velocity verlies met penetratie = " + (rb.velocity - (rb.velocity / other.GetComponent<Village>().Divider())) / ShopManager.penetration);
            rb.velocity -= ((rb.velocity - (rb.velocity / other.GetComponent<Village>().Divider())) / ShopManager.penetration);
            Debug.Log("nieuwe velocity = " + rb.velocity);
            if (ShopManager.penetration >= other.GetComponent<Village>().ReqPenetration())
            {
                other.GetComponent<Village>().Destroy();
            }
                   
        }

        if (other.gameObject.CompareTag("Explosive"))
        {
            other.gameObject.GetComponent<Explosive>().Explode();
        }
    }
    public void Delete()
    {
        Destroy(indicator);
        Destroy(gameObject);
    }

}
