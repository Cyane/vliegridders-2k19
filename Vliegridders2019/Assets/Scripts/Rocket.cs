﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour
{

    public float explosionRadius;
    public float explosionForce;
    public float flyingSpeed;
    public ParticleSystem blast;
    private Rigidbody rb;
    // Use this for initialization
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        rb.velocity = rb.transform.up * flyingSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (transform.position.z >= 0)
            Explode();
    }
    void OnDrawGizmos()//radius van de explosie
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }

    void Explode()
    {
        ParticleSystem.Instantiate(blast, transform.position, Quaternion.Euler(-90, 0, 0));//BOOM

        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, explosionRadius);
        foreach (Collider hit in colliders)
        {
            if (hit.gameObject.CompareTag("Settlement"))//dit blaast de huisjes op
            {
                hit.gameObject.GetComponent<Village>().Destroy();
            }

            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
                rb.AddExplosionForce(explosionForce, explosionPos, explosionRadius, 0.5f);
        }
        Destroy(gameObject);
        
    }
}
