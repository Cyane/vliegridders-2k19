﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShopManager : MonoBehaviour
{
	[Header("Prefabs")]
	public GameObject toggle;
    public GameObject upgradeButton;
    public Button closeShopButton;
    public Button retryButton;

    public Text knightButtonText;

    public GameObject cannonUpgradeGroup;
    public Text cannonButtonText;
    private GameObject[] cannonToggles;

    public GameObject penetrationUpgradeGroup;
    public Text penetrationButtonText;
    private GameObject[] penetrationToggles;

    public GameObject rocketUpgradeGroup;
    public Text rocketButtonText;
    private GameObject[] rocketToggles;

    [HideInInspector] public static int cannonCurrentUpgrade = 0;
    [HideInInspector] public static int penetrationCurrentUpgrade = 0;
    [HideInInspector] public static int rocketCurrentUpgrade = 0;
    [HideInInspector] private int totalUpgrades;

	[Header("Knight variables")]
	public int knightStartPrice = 5;
    public int knightPriceIncrement = 5;
    private int knightCurrentPrice;

	[Header("Cannon Upgrade variables")]
	int cannonUpgrades = 8;
    public int cannonStartPrice = 5;
    public int cannonPriceIncrement = 5;
    private int cannonCurrentPrice;
    public float cannonPowerStart;
    public float cannonPowerPerUpgrade;
    public static float cannonPower;
	[Header("penetration variables")]
	int penetrationUpgrades = 8;
    public int penetrationStartPrice = 5;
    public int penetrationPriceIncrement = 5;
    private int penetrationCurrentPrice;
    private float penetrationStart = 1;
    public float penetrationPerUpgrade;
    public static float penetration;

	[Header("Missile variables")]
	int rocketUpgrades = 3;
    public int rocketStartPrice = 50;
    public int rocketPriceIncrement = 25;
    private int rocketCurrentPrice;
    public int rocketsPerUpgrade;
    public static int rocketAmount;

    // Use this for initialization
    void Start()
    {
        UpdateKnightPrice();

        cannonPower = cannonPowerStart;
        cannonCurrentPrice = cannonStartPrice;

        cannonToggles = new GameObject[cannonUpgrades];
        for (int i = 0; i < cannonToggles.Length; i++)
        {
            GameObject upgradeCounter = (GameObject)Instantiate(toggle, transform.position, Quaternion.identity);
            upgradeCounter.transform.parent = cannonUpgradeGroup.transform;
            upgradeCounter.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
            cannonToggles[i] = upgradeCounter;
        }

        penetration = penetrationStart;
        penetrationCurrentPrice = penetrationStartPrice;

        penetrationToggles = new GameObject[penetrationUpgrades];
        for (int i = 0; i < penetrationToggles.Length; i++)
        {
            GameObject upgradeCounter = (GameObject)Instantiate(toggle, transform.position, Quaternion.identity);
            upgradeCounter.transform.parent = penetrationUpgradeGroup.transform;
            upgradeCounter.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            penetrationToggles[i] = upgradeCounter;
        }

        rocketAmount = 0;
        rocketCurrentPrice = rocketStartPrice;

        rocketToggles = new GameObject[rocketUpgrades];
        for (int i = 0; i < rocketToggles.Length; i++)
        {
            GameObject upgradeCounter = (GameObject)Instantiate(toggle, transform.position, Quaternion.identity);
            upgradeCounter.transform.parent = rocketUpgradeGroup.transform;
            upgradeCounter.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            rocketToggles[i] = upgradeCounter;
        }
    }

    void Update()
    {
        UpdateButtons();
    }

    public void UpdateKnightPrice()
    {
        totalUpgrades = cannonCurrentUpgrade + penetrationCurrentUpgrade + rocketCurrentUpgrade;
        knightCurrentPrice = knightStartPrice + (totalUpgrades * knightPriceIncrement);
    }

    public void BuyKnight()
    {
        if(GameManager.score >= knightCurrentPrice)
        {
            GameManager.score -= knightCurrentPrice;
            GameManager.knightsLeft++;
        }
    }

    public void UpgradeCannon()
    {
        if (cannonCurrentUpgrade < cannonUpgrades && GameManager.score >= cannonCurrentPrice)
        {
            GameManager.score -= cannonCurrentPrice;
            cannonCurrentPrice += cannonPriceIncrement;
            cannonCurrentUpgrade++;
            cannonToggles[cannonCurrentUpgrade - 1].GetComponent<Toggle>().isOn = true;

            cannonPower = cannonPowerStart + (cannonPowerPerUpgrade * cannonCurrentUpgrade);

            UpdateKnightPrice();
        }
    }

    public void UpgradePenetration()
    {
        if (penetrationCurrentUpgrade < penetrationUpgrades && GameManager.score >= penetrationCurrentPrice)
        {
            GameManager.score -= penetrationCurrentPrice;
            penetrationCurrentPrice += penetrationPriceIncrement;
            penetrationCurrentUpgrade++;
            penetrationToggles[penetrationCurrentUpgrade - 1].GetComponent<Toggle>().isOn = true;

            penetration = penetrationStart + (penetrationPerUpgrade * penetrationCurrentUpgrade);

            UpdateKnightPrice();
        }
    }

    public void UpgradeRockets()
    {
        if (rocketCurrentUpgrade < rocketUpgrades && GameManager.score >= rocketCurrentPrice)
        {
            GameManager.score -= rocketCurrentPrice;
            rocketCurrentPrice += rocketPriceIncrement;
            rocketCurrentUpgrade++;
            rocketToggles[rocketCurrentUpgrade - 1].GetComponent<Toggle>().isOn = true;

            rocketAmount = rocketsPerUpgrade * rocketCurrentUpgrade;

            UpdateKnightPrice();
        }
    }

    void UpdateButtons()
    {
        knightButtonText.text = ("Recruit new knight: " + knightCurrentPrice + "G");

        //cannon upgrade button
        if (cannonCurrentUpgrade >= cannonUpgrades)
        {
            cannonButtonText.text = ("Upgrade cannon power: <color=red>MAXED</color>");
        }
        else
        {
            cannonButtonText.text = ("Upgrade cannon power: " + cannonCurrentPrice + "G");
        }

        //armor upgrade button 
        if (penetrationCurrentUpgrade >= penetrationUpgrades)
        {
            penetrationButtonText.text = ("Upgrade armor: <color=red>MAXED</color>");
        }
        else
        {
            penetrationButtonText.text = ("Upgrade armor: " + penetrationCurrentPrice + "G");
        }

        //rocket upgrade button
        if (rocketCurrentUpgrade >= rocketUpgrades)
        {
            rocketButtonText.text = ("Upgrade rockets: <color=red>MAXED</color>");
        }
        else
        {
            rocketButtonText.text = ("Upgrade rockets: " + rocketCurrentPrice + "G");
        }

        if(GameManager.knightsLeft <= 0)
        {
            closeShopButton.interactable = false;
            closeShopButton.GetComponentInChildren<Text>().text = "<color=red>NO KNIGHTS LEFT</color>";
            retryButton.interactable = false;
            retryButton.GetComponentInChildren<Text>().text = "<color=red>NO KNIGHTS LEFT</color>";
        }
        else
        {
            closeShopButton.interactable = true;
            closeShopButton.GetComponentInChildren<Text>().text = "Done shopping!";
            retryButton.interactable = true;
            retryButton.GetComponentInChildren<Text>().text = "Retry";
        }

    }

}
