﻿using UnityEngine;
using System.Collections;

public class Village : MonoBehaviour {

    public float velocityDivider;
    public float requiredPenetration;
    public ParticleSystem blast;
    public ParticleSystem coin;

    // Use this for initialization
    void Start () {
	    
	}

    public void Destroy()
    {
        ParticleSystem.Instantiate(blast, transform.position, Quaternion.Euler(-90, 0, 0));
        int points = Random.Range(1, 4);
        for (int i = 0; i < points; i++)
        {
            ParticleSystem.Instantiate(coin, transform.position, Quaternion.Euler(-90,0,0));

        }
        GameManager.score += points;
        gameObject.SetActive(false);

    }
    public float Divider()
    {
        return velocityDivider;
    }

    public float ReqPenetration()
    {
        return requiredPenetration;
    }
}
