﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldSpawner : MonoBehaviour {

    public GameObject village1;
    public GameObject village2;
    public GameObject floor;
    [HideInInspector]
    public List<GameObject> villages;
    public float worldLength;
    public float tileSize;
    public float minSizeBetweenVillages;
    public float maxSizeBetweenVillages;
    private int numberOfTiles;
    private float tileDistance;
    private Vector3 gizmoTarget;

    // Use this for initialization
    void Start()
    {
        SpawnVillages();

        tileDistance = tileSize * floor.transform.localScale.x;
        numberOfTiles = (int)(worldLength / tileDistance) + 1;
        for (int i = 0; i < numberOfTiles; i++) //CREATE THE FLOOR
        {
            GameObject.Instantiate(floor, transform.position + new Vector3(tileDistance * i, 0, 0), Quaternion.identity);
        }
    }

    void SpawnVillages()
    {

        villages = new List<GameObject>();

        float currentX = 0;
        while (currentX < worldLength)//SPAWNS THE VILLAGES
        {
            currentX += Random.Range(minSizeBetweenVillages, maxSizeBetweenVillages);
            GameObject noobVillage = (GameObject)Instantiate(village1, transform.position + new Vector3(currentX, 0.5f, 0), Quaternion.identity);
            villages.Add(noobVillage);

        }
    }

    public void ResetVillages()
    {
        foreach(GameObject noobVillage in villages)
        {
            Destroy(noobVillage);
        }
        SpawnVillages();
    }

    void OnDrawGizmos()
    {
        gizmoTarget = new Vector3(tileDistance * numberOfTiles, 0, 0);
        if (gizmoTarget != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, gizmoTarget);
        }
    }

}
